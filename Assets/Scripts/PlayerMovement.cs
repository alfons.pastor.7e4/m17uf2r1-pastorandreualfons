using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public float JumpHeight = 1.2f;
    public float Gravity = -15.0f;
    public float JumpTimeout = 0.50f;
    public float FallTimeout = 0.15f;
    public bool Grounded = true;
    public float GroundedOffset = -0.14f;
    public float GroundedRadius = 0.4f;
    public LayerMask GroundLayers;
    public GameObject bulletPrefab;
    public float TopClamp = 70.0f;
    public float BottomClamp = -30.0f;
    public GameObject CinemachineCameraTarget;
    public GameObject ThirdPersonVCam;
    public GameObject AimVCam;
    public GameObject FaceFollowVCam;
    
    private InputHandler _input;
    private CharacterController _characterController;
    private Animator _animator;
    [SerializeField] private float _moveSpeed = 7;
    [SerializeField] private float _runSpeed = 9;
    private float _currentSpeed;
    [SerializeField] private float rotateSpeed = 180;
    private float _animatorSpeed = 0f;
    private bool _weaponEquiped = false;
    private bool _isInteracting = false;
    private float _verticalVelocity = 0;
    private GameObject _weaponPoint;
    private bool _weaponIdle = true;
    private float _terminalVelocity = 53.0f;
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;

    private float _jumpTimeoutDelta;
    private float _fallTimeoutDelta;

    private Vector3 _playerVelocity;

    // Start is called before the first frame update
    void Start()
    {
        _weaponPoint = GameObject.FindWithTag("WeaponPoint");
        _weaponPoint.SetActive(false);
        _input = GetComponent<InputHandler>();
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        JumpAndGravity();
        GroundedCheck();
        FiringCheck();

        Move();
    }

    private void FixedUpdate()
    {
    }

    private void LateUpdate()
    {
        CameraRotation();
    }

    private void GroundedCheck()
    {
        // set sphere position, with offset
        Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
            transform.position.z);
        Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);

        // update animator if using character
        /*if (_hasAnimator)
        {
            _animator.SetBool(_animIDGrounded, Grounded);
        }*/
    }

    private void FiringCheck()
    {
        if (_input.firing && _weaponEquiped)
        {
            GameObject bullet =
                Instantiate(bulletPrefab, _weaponPoint.transform.position, gameObject.transform.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * 2000);
            /*_animator.SetBool("Fire", true);
            if (_weaponIdle)
            {
                _weaponIdle = false;
                _weaponPoint.transform.Rotate(Vector3.forward, -90);
                _weaponPoint.transform.Rotate(Vector3.right, 10);
            }
        }
        else
        {
            _animator.SetBool("Fire", false);
            if (!_weaponIdle)
            {
                _weaponIdle = true;
                _weaponPoint.transform.Rotate(Vector3.forward, 90);
                _weaponPoint.transform.Rotate(Vector3.right, -10);

            }*/
        }
    }

    void Move()
    {
        _currentSpeed = _input.running ? _runSpeed : _moveSpeed;
        Vector3 rotation = new Vector3(0, _input.move.x * rotateSpeed * Time.deltaTime, 0);
        Vector3 move = new Vector3(0, 0, _input.move.y * Time.deltaTime);
        move = transform.TransformDirection(move);
        move.y = _verticalVelocity * Time.deltaTime;
        _characterController.Move(move * _currentSpeed);
        transform.Rotate(rotation);
        _animatorSpeed = Mathf.Lerp(_animatorSpeed, _input.move.magnitude * _currentSpeed, 0.05f);
        _animator.SetFloat("Speed", _animatorSpeed);
    }

    public void EquipWeapon()
    {
        _weaponEquiped = !_weaponEquiped;
        _animator.SetLayerWeight(1, _weaponEquiped ? 1 : 0);
        _weaponPoint.SetActive(_weaponEquiped);
        ThirdPersonVCam.SetActive(!_weaponEquiped);
        AimVCam.SetActive(_weaponEquiped);
    }

    public void CameraRotation()
    {
        _cinemachineTargetYaw += _input.look.x * Time.deltaTime;

        _cinemachineTargetPitch += _input.look.y * Time.deltaTime;

        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
            _cinemachineTargetYaw, 0.0f);
    }

    private void JumpAndGravity()
    {
        Debug.Log("Grounded: " + Grounded);
        Debug.Log("Jump: " + _input.jumping);
        if (Grounded)
        {
            // reset the fall timeout timer
            _fallTimeoutDelta = FallTimeout;

            // update animator if using character
            /*if (_hasAnimator)
            {
                _animator.SetBool(_animIDJump, false);
                _animator.SetBool(_animIDFreeFall, false);
            }*/

            // stop our velocity dropping infinitely when grounded
            if (_verticalVelocity < 0.0f)
            {
                _verticalVelocity = -2f;
            }

            // Jump
            if (_input.jumping && _jumpTimeoutDelta <= 0.0f)
            {
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);

                // update animator if using character
                /*if (_hasAnimator)
                {
                    _animator.SetBool(_animIDJump, true);
                }*/

                _animator.SetTrigger("Jump");
            }

            // jump timeout
            if (_jumpTimeoutDelta >= 0.0f)
            {
                _jumpTimeoutDelta -= Time.deltaTime;
            }
        }
        else
        {
            // reset the jump timeout timer
            _jumpTimeoutDelta = JumpTimeout;

            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= Time.deltaTime;
            }
            else
            {
                // update animator if using character
                /* if (_hasAnimator)
                 {
                     _animator.SetBool(_animIDFreeFall, true);
                 }*/
            }

            // if we are not grounded, do not jump
            _input.jumping = false;
        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        if (_verticalVelocity < _terminalVelocity)
        {
            _verticalVelocity += Gravity * Time.deltaTime;
        }
    }

    public void Celebrate()
    {
        _animator.SetTrigger("Victory");
        StartCoroutine(InputFreeze(5f));
        StartCoroutine(CelebrationCam());
    }

    public void Interact()
    {
        _isInteracting = !_isInteracting;
        _animator.SetLayerWeight(2, _isInteracting ? 1 : 0);
        if (_isInteracting) _animator.SetTrigger("Interact");
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, 0.5f, Vector3.up);
        foreach (RaycastHit h in hits)
        {
            if (h.collider.CompareTag("Pickable"))
            {
                PickObject(h.collider.gameObject);
            }
        }
    }

    private void PickObject(GameObject gameObject)
    {
        gameObject.transform.parent = _weaponPoint.transform;
        gameObject.transform.localPosition = Vector3.zero;
    }
    
    IEnumerator CelebrationCam()
    {
        ThirdPersonVCam.SetActive(false);
        AimVCam.SetActive(false);
        FaceFollowVCam.SetActive(true);
        yield return new WaitForSeconds(5f);
        FaceFollowVCam.SetActive(false);
        ThirdPersonVCam.SetActive(true);
    }

    IEnumerator InputFreeze(float time)
    {
        PlayerInput pi = GetComponent<PlayerInput>();
        pi.DeactivateInput();
        yield return new WaitForSeconds(time);
        pi.ActivateInput();
    }
    public static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
}