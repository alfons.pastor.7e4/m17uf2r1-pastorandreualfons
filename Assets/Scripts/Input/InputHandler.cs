using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityStandardAssets.Characters.ThirdPerson;
using Vector2 = UnityEngine.Vector2;

public class InputHandler : MonoBehaviour
{
    private PlayerMovement _playerMovement;
    public Vector2 move;
    public bool running;
    public bool jumping;
    public bool firing;
    public Vector2 look;
    public AICharacterControl aicontrol;

    // Start is called before the first frame update

    public void OnMove(InputValue input)
    {
        move = input.Get<Vector2>();
    }

    public void OnRun(InputValue input)
    {
        running = input.Get<float>() > 0;
    }

    public void OnEquipWeapon(InputValue input)
    {
        _playerMovement.EquipWeapon();
    }

    public void OnJump(InputValue input)
    {
        jumping = input.isPressed;
    }

    public void OnCelebrate(InputValue input)
    {
        _playerMovement.Celebrate();
    }

    public void OnInteract(InputValue input)
    {
        _playerMovement.Interact();
    }

    public void OnFire(InputValue input)
    {
        firing = input.isPressed;
    }

    public void OnLook(InputValue input) => look = input.Get<Vector2>();

    public void OnAIMode(InputValue input)
    {
        //aicontrol.SwitchTarget();
    }

    void Start()
    {
        _playerMovement = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
    }
}