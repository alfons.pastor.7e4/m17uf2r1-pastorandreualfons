using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [Serializable]
    public enum AIState
    {
        Patrol,
        Persecute,
        Attack
    }

    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent
            agent { get; private set; } // the navmesh agent required for the path finding

        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform playerTarget;
        public Transform staticTarget; // target to aim for
        public Transform target;
        public Transform[] patrolTargets;
        private int patrolIndex = 0;
        private bool chasePlayer = true;
        private bool attacking = false;
        public AIState state;
        private bool onTarget;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;
            state = AIState.Patrol;
            target = patrolTargets[patrolIndex];
            agent.SetDestination(target.position);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && state != AIState.Attack)
            {
                state = AIState.Persecute;
            }
        }

        private void Update()
        {
            onTarget = agent.remainingDistance <= agent.stoppingDistance;
            switch (state)
            {
                case AIState.Patrol:
                    if (onTarget)
                    {
                        if (patrolIndex < patrolTargets.Length - 1)
                        {
                            patrolIndex++;
                        }
                        else
                        {
                            patrolIndex = 0;
                        }

                        target = patrolTargets[patrolIndex];
                    }

                    break;
                case AIState.Persecute:
                    target = playerTarget;
                    chasePlayer = true;
                    if (onTarget)
                    {
                        state = AIState.Attack;
                    }
                    break;
                case AIState.Attack:
                    attacking = true;
                    if (!onTarget)
                    {
                        attacking = false;
                        state = AIState.Persecute;
                    }
                    break;
            }

            if (target != null)
                agent.SetDestination(target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false, attacking);
            else
                character.Move(Vector3.zero, false, false, attacking);
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        public void SwitchTarget()
        {
            chasePlayer = !chasePlayer;
            target = chasePlayer ? playerTarget : staticTarget;
        }
    }
}