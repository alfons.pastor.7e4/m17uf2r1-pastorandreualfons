# M17UF2R1.1

## Controls

* Move -> **WASD**
* Run -> **Hold Shift**
* Jump -> **Space**
* Celebration -> **V**
* Equip Weapon -> **X**
* Fire -> **Left Mouse Button**
* Interact -> **E**
* Camera Look -> **Mouse**